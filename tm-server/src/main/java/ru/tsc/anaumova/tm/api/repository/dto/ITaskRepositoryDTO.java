package ru.tsc.anaumova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.anaumova.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskRepositoryDTO extends IUserOwnedRepositoryDTO<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeTasksByProjectId(String projectId);

}